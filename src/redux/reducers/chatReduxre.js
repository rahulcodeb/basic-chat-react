import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

export const actionTypes = {
  SetChatState: '[SetChatState] Action',
  ResetChatState: '[ResetChatState] Action',
}

const initialChatState = {
  data: {}
}

export const ChatReducers = persistReducer(
  {
    storage,
    key: 'v706-chat-state',
    blacklist: [],
  },
  (state = initialChatState, action) => {
    switch (action.type) {
      case actionTypes.SetChatState: {
        const data = action.payload
        if (!data) return state
        const { id, chat } = data;
        return {
          data: {
            ...state.data,
            [id]: [
              ...(state.data[id] || []),
              chat,
            ]
          }
        }
      }

      case actionTypes.ResetChatState: {
        return initialChatState
      }

      default:
        return state
    }
  },
)

export const ChatActions = {
  SetChatState: (payload) => ({
    type: actionTypes.SetChatState,
    payload,
  }),
  ResetChatState: () => ({
    type: actionTypes.ResetChatState,
  }),
}
