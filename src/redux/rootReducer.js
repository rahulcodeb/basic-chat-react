import { all } from 'redux-saga/effects'
import { combineReducers } from 'redux'
import { ChatReducers } from './reducers/chatReduxre'

export const rootReducer = combineReducers({
  chatData: ChatReducers
})

export function* rootSaga() {
  yield all([
    // auth.saga()
  ])
}
