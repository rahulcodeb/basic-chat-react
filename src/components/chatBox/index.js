import React from 'react'
import { shallowEqual, useSelector } from 'react-redux'
import './ChatBox.css'

const ChatBox = () => {
  const { chats } = useSelector(
    (store) => ({
      chats: store.chatData.data.user1,
    }),
    shallowEqual,
  )

  console.log(chats);
  return (
    <>
      <div className="ChatBox">
        {
          (chats || []).map(e => {
            return (
              <>
                <div className="ChatBox-Chat">
                  <span>
                    {
                      e
                    }
                  </span>
                </div>
              </>
            )
          })
        }
      </div>
    </>
  )
}

export default ChatBox
