import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { ChatActions } from '../../redux/reducers/chatReduxre'
import './ChatControls.css'

const ChatControls = () => {
  const [message, setMessage] = useState('')
  const dispatch = useDispatch()
  return (
    <>
      <div className="ChatControls">
        <input
          type="text"
          value={message}
          onChange={(e) => setMessage(e.target.value)}
          onKeyUp={(e) => {
            if (e.key === 'Enter' && message) {
              dispatch(ChatActions.SetChatState({
                id: 'user1',
                chat: message
              }))
              setMessage('')
            }
          }}
        />
      </div>
    </>

  )
}

export default ChatControls
