import ChatBox from "./components/chatBox";
import ChatControls from "./components/chatControls";

function App() {
  return (
    <>
      <ChatBox />
      <ChatControls />
    </>
  );
}

export default App;
